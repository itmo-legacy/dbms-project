-- A user can view information from this table, choose the method that
-- works for him and purchase a wallet with this method's id via
-- "createRidesWallet".
--
-- Also, postgres docs say that in group by selects I can use
-- attributes that are functionally dependent on attributes being
-- grouped by.
--
-- Isolation level: Read Committed. Read Uncommitted doesn't work for
-- us, because we don't want to see uncommitted, unchecked data that
-- might be invalid. Read Committed works for us, because we don't
-- read anything two times, we just present information that is there,
-- one row at a time.
create or replace view RidesPaymentMethodInfo as
  select R.MethodId,
         R.Name,
         string_agg(distinct C.Name, ', ' order by C.Name) as "Can be used for",
         R.Price as "Total Price",
         R.TotalRides as "Total Rides",
         cast(R.Price / R.TotalRides as decimal(9, 2)) as "Price per Ride"
    from RidesPaymentMethod R
           natural join RidesProviderCategory P
           join TransportCategory C on P.CategoryId = C.CategoryId
   group by R.MethodId;

-- Isolation level: Read Committed. Same reasoning as for
-- RidesPaymentMethodInfo.
create or replace view TimePaymentMethodInfo as
  select T.MethodId,
         T.Name,
         string_agg(distinct C.Name, ', ' order by C.Name) as "Can be used for",
         T.Price as "Total Price",
         T.Duration
    from TimePaymentMethod T
           natural join TimeProviderCategory P
           join TransportCategory C on P.categoryid = C.Categoryid
   group by T.MethodId;

-- Isolation level: Read Committed. Read Uncommitted doesn't work,
-- because of reasons above. I expect IDs not to change and other than
-- that we don't read things twice.
create or replace function getProviderCategoryForMoneyMethod(TheMethodId int)
  returns table (
    ProviderId int,
    CategoryId int,
    Price decimal(9, 2)
  )
as $$
begin
  return query
    -- not materialized by default, will be included as-is into parent
    -- query, can be optimized.
    with ProvPrices as (
      select P.ProviderId, P.CategoryId, P.Price
        from ProviderCategoryPrice P
       where MethodId = TheMethodId
    ), CatPrices as (
      select C.CategoryId, C.Price
        from GovernmentCategoryPrice C
       where MethodId = TheMethodId
    )
    (select PC.ProviderId, PC.CategoryId, coalesce(P.Price, C.Price)
       from ProviderCategory PC
              left outer join ProvPrices P
                  on PC.ProviderId = P.ProviderId
                  and PC.CategoryId = P.CategoryId
              left outer join CatPrices C
                  on PC.CategoryId = C.CategoryId
      where coalesce(P.Price, C.Price) is not null);
end;
$$ language plpgsql;

-- Isolation Level: Read Committed. We expect IDs not to change. If
-- some provider decides to change a name in the middle of a query —
-- so be it, I'll judge it to have __happenned before__ the
-- request. So it's okay.
create or replace function getMoneyPaymentMethodInfo(TheMethodId int)
  returns table (
    ProviderId int,
    ProviderName varchar(120),
    CategoryName varchar(120),
    Price decimal(9, 2)
  )
as $$
begin
  return query
    (select distinct M.ProviderId, P.Name, C.Name, M.Price
       from getProviderCategoryForMoneyMethod(TheMethodId) M
              inner join TransportProvider P on P.ProviderId = M.ProviderId
              inner join TransportCategory C on C.CategoryId = M.CategoryId);
end;
$$ language plpgsql;
