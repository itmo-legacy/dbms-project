-- Isolation level: Repeatable Read. We write data, so Read
-- Uncommitted doesn't fit. Read Committed might lead to key conflict,
-- when two transaction see the same MinKey and overwrite it
-- twice. With Repeatable Read, once we've read MinKey, it doesn't
-- change. AFAIR we take a lock at the MinKey location. So, we safely
-- overwrite that same MinKey that we saw. It's not incremented by
-- someone who also saw the same MinKey.
create or replace function fetchNewWalletKey(WalletTypeName varchar(10))
  returns int
as $$
declare
  TheKey int;
begin
  TheKey := (select MinKey from WalletKeys where TypeName = walletTypeName);
  update WalletKeys set MinKey = MinKey + 1 where TypeName = walletTypeName;
  return TheKey;
end;
$$ language plpgsql;

-- Isolation level: Repeatable Read. First of all, it's at least RR,
-- because we use "fetchNewWalletKey". We expect TotalRides not to
-- change. In our system you create a new method instead.
create or replace function createRidesWallet(RidesMethodId int)
  returns int
as $$
declare
  newKey int default fetchNewWalletKey('rides');
  totalRides numeric(4);
begin
  totalRides := (select R.TotalRides
                   from RidesPaymentMethod R
                  where MethodId = RidesMethodId);
  insert into RidesWallet(WalletId, RidesLeft, MethodId)
  values (newKey, totalRides, ridesMethodId);
  return newKey;
end;
$$ language plpgsql;

-- Isolation level: Repeatable Read. First of all, it's at least RR,
-- because we use "fetchNewWalletKey". We just do one insert, we don't
-- care about possible new records.
create or replace function createTimeWallet(TimeMethodId int)
  returns int
as $$
declare
  newKey int default fetchNewWalletKey('time');
begin
  insert into TimeWallet(WalletId, MethodId)
  values (newKey, timeMethodId);
  return newKey;
end;
$$ language plpgsql;

-- Isolation level: Repeatable Read. First of all, it's at least RR,
-- because we use "fetchNewWalletKey". We just do one insert, we don't
-- care about possible new records.
create or replace function createMoneyWallet(
  moneyMethodId int,
  initialBalance decimal(9, 2)
)
  returns int
as $$
declare
  newKey int default fetchNewWalletKey('money');
begin
  insert into MoneyWallet(WalletId, Balance, MethodId)
  values (newKey, initialBalance, moneyMethodId);
  return newKey;
end;
$$ language plpgsql;

-- Isolation level: Read Committed. We write, so no Read
-- Uncommitted. We don't basically read anything, just one update.
create or replace procedure topUpMoneyWallet(
  TheWalletId int,
  Addition decimal(9, 2)
)
as $$
begin
  if Addition < 0 then
    rollback;
  end if;
  update MoneyWallet set Balance = Balance + Addition
   where WalletId = TheWalletId;
end;
$$ language plpgsql;

-- Isolation Level: Read Committed. We just delete stuff.
create or replace procedure purgeExpiredWallets()
as $$
begin
  delete from RidesWallet
   where RidesLeft = 0;
  delete from TimeWallet
   where WalletId in (select WalletId
                        from TimeWallet I natural join TimePaymentMethod
                       where I.CreationTime + Duration < now());
end;
$$ language plpgsql;
