-- Absolutely not portable, but I need a way to make people now that they need
-- to evaluate "update.sql" first.
\echo Please, first evaluate "updates.sql", this files relies on some functions from it


---- Payment Methods ----

insert into RidesPaymentMethod(MethodId, Name, TotalRides, Price)
values (1, 'Автобусный', 2, 70),
       (2, 'Автобусный', 5, 165),
       (3, 'Автобусный', 20, 620),
       (4, 'Метро', 2, 85),
       (5, 'Метро', 5, 205),
       (6, 'Метро', 20, 700),
       -- 'Рельсовый' method for ecologically superiour transport
       (7, 'Рельсовый', 30, 1100), -- metro/tram,
       (8, 'Рельсовый', 30, 900), -- tram only
       (9, 'Единый', 30, 1200); -- metro/tram/bus

-- Here I use postgres-specific time format because it's much clearer
-- than the SQL standard format. Also I have enough postgres-specific
-- things to be unafraid to use another one.

insert into TimePaymentMethod(MethodId, Name, Duration, Price)
values (1, 'Наземный', '90 minutes', 70), -- tram/bus
       (2, 'Наземный', '1 month', 3100),
       (3, 'Ромашка', '1 month', 3800), -- metro/tram/bus
       (4, 'Метро', '90 minutes', 90),
       (5, 'Метро', '1 month', 3400);

insert into MoneyPaymentMethod(MethodId, Name, Price)
values (1, 'Подорожник', 60), -- metro/tram/bus
       (2, 'Рельсовый', 100); -- metro/tram, ecologically superior

---- Transport ----

insert into TransportCategory(CategoryId, Name, GovernmentCashPrice)
values (1, 'Автобус', 55),
       (2, 'Трамвай', 55),
       (3, 'Метро', 60),
       (4, 'Гиперпетля', null),
       (5, 'Маршрутное такси', null);

insert into TransportProvider(ProviderId, Name)
values (1, 'Павловский автопарк'),
       (2, 'Приморский автопарк'),
       (3, 'Транспортная концессионная компания'),
       (4, 'Горэлектротранс'),
       (5, 'Петербургский метрополитен'),
       (6, 'ИП Илон Маск'),
       (7, 'ИП Кудрявочкин А.О.'),
       (8, 'ИП Булаев Х.Ш.'),
       (9, 'ИП Кудрявочкин А.О.');

insert into ProviderCategory(ProviderId, CategoryId, ProviderCashPrice)
values (1, 1, null),
       (1, 2, null),
       (2, 1, null),
       (3, 2, 60), -- they are a private company.
       (4, 2, null),
       (5, 3, null),
       (6, 4, 200), -- a quick way to get from city south to city north.
       (6, 5, 65), -- profitable, unlike hyperloop.
       (7, 5, 60),
       (8, 5, 40),
       (9, 5, 110);

---- Method to Transport Connections ----

insert into RidesProviderCategory(ProviderId, CategoryId, MethodId)
values
  -- buses
  (1, 1, 1),
  (2, 1, 1),
  (1, 1, 2),
  (2, 1, 2),
  (1, 1, 3),
  (2, 1, 3),
  (1, 1, 9),
  (2, 1, 9),
  -- trams
  (4, 2, 7),
  (4, 2, 8),
  (4, 2, 9),
  -- metro
  (5, 3, 4),
  (5, 3, 5),
  (5, 3, 6),
  (5, 3, 7),
  (5, 3, 9);

insert into TimeProviderCategory(ProviderId, CategoryId, MethodId)
values
  -- buses
  (1, 1, 1),
  (2, 1, 1),
  (1, 1, 2),
  (2, 1, 2),
  (1, 1, 3),
  (2, 1, 3),
  -- trams
  (4, 2, 1),
  (4, 2, 2),
  (4, 2, 3),
  -- metro
  (5, 3, 3),
  (5, 3, 4),
  (5, 3, 5);

insert into ProviderCategoryPrice(ProviderId, CategoryId, MethodId, Price)
values
  -- Подорожник
  (3, 2, 1, 60),
  (7, 5, 1, 60),
  (8, 5, 1, 40),
  (9, 5, 1, 105),
  -- Рельсовый
  (3, 2, 2, 55),
  (6, 4, 2, 180);

insert into GovernmentCategoryPrice(CategoryId, MethodId, Price)
values
  -- Подорожник
  (1, 1, 36),
  (2, 1, 36),
  (3, 1, 41),
  -- Рельсовый
  (2, 2, 30),
  (3, 2, 38);

---- Create some wallets ----

select createRidesWallet(1);
select createRidesWallet(6);
select createRidesWallet(9);

select createTimeWallet(3);

select createMoneyWallet(1, 500.00);
select createMoneyWallet(2, 650.00);
