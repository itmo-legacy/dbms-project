create table RidesPaymentMethod (
  MethodId int not null,
  Name varchar(100) not null,
  TotalRides numeric(4) not null,
  Price decimal(9, 2) not null,
  primary key (MethodId),
  check (TotalRides > 0),
  check (Price >= 0)
);

create table RidesWallet (
  WalletId int not null,
  RidesLeft numeric(4) not null,
  CreationTime timestamp not null default now(),
  MethodId int not null,
  primary key (WalletId),
  foreign key (MethodId) references RidesPaymentMethod(MethodId),
  check (RidesLeft >= 0)
);

create table TimePaymentMethod (
  MethodId int not null,
  Name varchar(100) not null,
  Duration interval not null,
  Price decimal(9, 2) not null,
  primary key (MethodId),
  check (Duration > '0 seconds'),
  check (Price >= 0)
);

create table TimeWallet (
  WalletId int not null,
  CreationTime timestamp not null default now(),
  MethodId int not null,
  primary key (WalletId),
  foreign key (MethodId) references TimePaymentMethod(MethodId)
);

create table MoneyPaymentMethod (
  MethodId int not null,
  Name varchar(100) not null default now(),
  Price decimal(9, 2) not null,
  primary key (MethodId),
  check (Price >= 0)
);

create table MoneyWallet (
  WalletId int not null,
  Balance decimal(9, 2) not null,
  CreationTime timestamp not null default now(),
  MethodId int not null,
  primary key (WalletId),
  foreign key (MethodId) references MoneyPaymentMethod(MethodId),
  check (Balance >= 0)
);

create table TransportProvider (
  ProviderId int not null,
  Name varchar(120) not null,
  primary key (ProviderId)
);

create table TransportCategory (
  CategoryId int not null,
  Name varchar(120) not null,
  GovernmentCashPrice decimal(9, 2),
  primary key (CategoryId),
  unique (Name),
  check (coalesce(GovernmentCashPrice, 0) >= 0)
);

create table ProviderCategory (
  ProviderId int not null,
  CategoryId int not null,
  ProviderCashPrice decimal(9, 2),
  primary key (ProviderId, CategoryId),
  foreign key (ProviderId) references TransportProvider(ProviderId),
  foreign key (CategoryId) references TransportCategory(CategoryId),
  check (coalesce(ProviderCashPrice, 0) >= 0)
);

create table RidesProviderCategory (
  ProviderId int not null,
  CategoryId int not null,
  MethodId int not null,
  primary key (ProviderId, CategoryId, MethodId),
  foreign key (ProviderId, CategoryId) references ProviderCategory(ProviderId, CategoryId),
  foreign key (MethodId) references RidesPaymentMethod(MethodId)
);

create table TimeProviderCategory (
  ProviderId int not null,
  CategoryId int not null,
  MethodId int not null,
  primary key (ProviderId, CategoryId, MethodId),
  foreign key (ProviderId, CategoryId) references ProviderCategory(ProviderId, CategoryId),
  foreign key (MethodId) references TimePaymentMethod(MethodId)
);

create table ProviderCategoryPrice (
  ProviderId int not null,
  CategoryId int not null,
  MethodId int not null,
  Price decimal(9, 2) not null,
  primary key (ProviderId, CategoryId, MethodId),
  foreign key (ProviderId, CategoryId) references ProviderCategory(ProviderId, CategoryId),
  foreign key (MethodId) references MoneyPaymentMethod(MethodId),
  check (Price >= 0)
);

create table GovernmentCategoryPrice (
  CategoryId int not null,
  MethodId int not null,
  Price decimal(9, 2) not null,
  primary key (CategoryId, MethodId),
  foreign key (CategoryId) references TransportCategory(CategoryId),
  foreign key (MethodId) references MoneyPaymentMethod(MethodId),
  check (Price >= 0)
);

-- Utility table to generate keys for wallets. I decided not to use it
-- for other tables, because it's not a routine to add new payment
-- methods or new transport categories or new transport providers. On
-- the other hand, purchasing new wallets is the most usual thing
-- ever.

create table WalletKeys(
  TypeName varchar(10) not null,
  MinKey int not null,
  primary key (TypeName)
);

-- Although I'm performing inserts, it still feels like a part of ddl,
-- because we just define starting values for keys. There is a
-- constant number of rows in this table and it's (probably) never
-- going to change.
insert into WalletKeys (TypeName, MinKey)
values ('rides', 1),
       ('time', 1),
       ('money', 1);

---- Indices ----

-- It's good for fetchNewWalletKey, which we regularly use to make a
-- new id for a wallet. The index is covering, so we don't need to
-- look up MinKey on the disk.
create index covering_minkey_index on WalletKeys using btree(TypeName, MinKey);

-- TransportCategory and TransportProvider are rather
-- small. TransportCategory has probably not more than 10 values and
-- TransportProvider not more than 200. They don't have a lot of
-- atributes and should fit into memory completely. So, there is not
-- much sense in creating indices for them, AFAIS.

-- The same is true for *PaymentMethod. There is usually a small
-- number of payment methods, otherwise it would be difficult to study
-- them and buy an appropriate one. Probably, less than 30. So, the
-- same goes for them.

-- On the other hand, there might be a lot of wallets, so let's create
-- indices on foreign keys of wallets.
create index rides_method_fk_index on RidesWallet using hash(MethodId);

create index time_method_fk_index on TimeWallet using hash(MethodId);

create index money_method_fk_index on MoneyWallet using hash(MethodId);
